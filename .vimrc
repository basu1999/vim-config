set nu
set cindent
set nocompatible
set shiftwidth=4 tabstop=4
set expandtab
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'junegunn/fzf.vim'
Plugin 'adelarsq/vim-matchit'
Plugin 'airblade/vim-gitgutter'
Plugin 'vim-syntastic/syntastic'
Plugin 'SirVer/ultisnips'
Plugin 'davidhalter/jedi-vim'



call vundle#end()
filetype plugin indent on

